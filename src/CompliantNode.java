import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;
import java.util.stream.Collectors;


/* CompliantNode refers to a node that follows the rules (not malicious)*/
public class CompliantNode implements Node {

    private boolean[] followees;
    private boolean[] blacklist;
    private Set<Transaction> pendingTransactions ;

    private double p_graph;    //the pairwise connectivity probability of the random graph: e.g. {.1, .2, .3}
    private double p_malicious; //the probability that a node will be set to be malicious: e.g {.15, .30, .45}
    private double p_txDistribution;  //the probability that each of the initial valid transactions will be communicated: e.g. {.01, .05, .10}
    private int numRounds ; //the number of rounds in the simulation e.g. {10, 20}

    private int currentRound = 0;

    public CompliantNode(double p_graph, double p_malicious, double p_txDistribution, int numRounds) {
        this.p_graph = p_graph;
        this.p_malicious = p_malicious;
        this.p_txDistribution = p_txDistribution;
        this.numRounds = numRounds;
    }

    public void setFollowees(boolean[] followees) {
        this.followees = followees;
        blacklist = new  boolean[followees.length];
        Arrays.fill(this.blacklist, false);
    }

    public void setPendingTransaction(Set<Transaction> pendingTransactions) {
        this.pendingTransactions = pendingTransactions;
    }

    public Set<Transaction> sendToFollowers() {
        return pendingTransactions;
    }

    public void receiveFromFollowees(Set<Candidate> candidates) {
        this.currentRound++;
        if(currentRound > numRounds)
            pendingTransactions.clear();

        checkMalicious(candidates);
        for(Candidate c: candidates){
        if(! blacklist[c.sender] && !pendingTransactions.contains(c.tx)){
            pendingTransactions.add(c.tx);
        }
        }

    }

    private void checkMalicious(Set<Candidate> candidates){
        Set<Integer> senders = candidates.stream().map(cand->  cand.sender).collect(Collectors.toSet());
        for(int i=0;i<followees.length; i++){
            if(followees[i] && !senders.contains(i))
                blacklist[i] = true;
        }
    }
}
